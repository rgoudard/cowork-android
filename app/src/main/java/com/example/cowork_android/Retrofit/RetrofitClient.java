package com.example.cowork_android.Retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit instance;

    public static Retrofit getInstance() {
        if (instance == null) {
            //In Emulator, localhost will be change to http://10.0.2.2:3000/
            instance = new Retrofit.Builder().baseUrl("http://51.91.10.171:3000/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return instance;
    }
}
