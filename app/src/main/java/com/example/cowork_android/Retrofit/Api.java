package com.example.cowork_android.Retrofit;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface Api {
    @FormUrlEncoded
    @POST("connection")
    Call<JsonElement> loginUser(@Field("email") String email,
                                @Field("password") String password);


    @FormUrlEncoded
    @PUT("users/{userId}")
    Call<JsonElement> updateUserInformations(@Path("userId") int userId,
                                             @Field("firstname") String firstname,
                                             @Field("lastname") String lastname,
                                             @Field("email") String email);

    @FormUrlEncoded
    @PUT("user/{userId}")
    Call<JsonElement> updateUserPassword(@Path("userId") int userId,
                                         @Field("password") String password);


    @GET("ticketNotAffected")
    Call<JsonElement> getNotAffectedTickets();

    @GET("tickets/{userId}")
    Call<JsonElement> getTicketsByUser(@Path("userId") int userId);

    @FormUrlEncoded
    @PUT("tickets/{ticketId}")
    Call<JsonElement> affectTicket(@Path("ticketId") int ticketId,
                                   @Field("idUserDev") int idUserDev);

    @GET("ticket/{ticketId}")
    Call<JsonElement> getTicketById(@Path("ticketId") int ticketId);

    @FormUrlEncoded
    @POST("messages")
    Call<JsonElement> createMessage(@Field("message") String message,
                                    @Field("idUser") int idUser,
                                    @Field("idTicket") int idTicket);

    @GET("messages/{ticketId}")
    Call<JsonElement> getMessageByTicket(@Path("ticketId") int ticketId);
}
