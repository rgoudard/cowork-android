package com.example.cowork_android.Models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {

    private int id, idUser, idTicket;
    String message, date;

    public Message() {
    }

    public Message(int id, String message, int idUser, int idTicket, String date){
        this.id = id;
        this.message = message;
        this.idUser = idUser;
        this.idTicket = idTicket;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public int getIdUser() {
        return idUser;
    }

    public int getIdTicket() {
        return idTicket;
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
        SimpleDateFormat strFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String dateString = "";

        try {
            Date date = dateFormat.parse(this.date);
            dateString = strFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateString;
    }
}
