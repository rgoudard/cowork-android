package com.example.cowork_android.Models;

public class User {
    private int id;
    private String lastName, firstName, type, email, password;

    public User() {

    }

    public User(int id, String firstName, String lastName, String email, String password, String type) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getType() {
        return type;
    }
}
