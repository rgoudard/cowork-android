package com.example.cowork_android.Models;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Ticket {

    private int id, idUser, idUserDev;
    private String title, subject, status, created_at;

    public Ticket() {
    }

    public Ticket(int id, int idUser, int idUserDev, String title, String subject, String status,
                  String created_at) {
        this.id = id;
        this.idUser = idUser;
        this.idUserDev = idUserDev;
        this.title = title;
        this.subject = subject;
        this.status = status;
        this.created_at = created_at;

    }

    public int getId() {
        return id;
    }

    public int getIdUser() {
        return idUser;
    }

    public int getIdUserDev() {
        return idUserDev;
    }

    public String getTitle() {
        return title;
    }

    public String getSubject() {
        return subject;
    }

    public String getStatus() {
        return status;
    }

    public String getCreated_at() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
        SimpleDateFormat strFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String dateString = "";

        try {
            Date date = dateFormat.parse(this.created_at);
            dateString = strFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateString;
    }
}
