package com.example.cowork_android.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.cowork_android.Adapters.MyTicketsAdapter;
import com.example.cowork_android.Models.Ticket;
import com.example.cowork_android.Models.User;
import com.example.cowork_android.R;
import com.example.cowork_android.Retrofit.Api;
import com.example.cowork_android.Retrofit.RetrofitClient;
import com.example.cowork_android.Storage.SharedPrefManager;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MyTicketsFragment extends Fragment {

    Api myApi;
    RecyclerView recyclerView;
    MyTicketsAdapter myTicketsAdapter;
    List<Ticket> tickets = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_tickets, container, false);

        //USER
        User user = SharedPrefManager.getInstance(getActivity()).getUser();

        // INIT API
        Retrofit retrofit = RetrofitClient.getInstance();
        myApi = retrofit.create(Api.class);

        Call<JsonElement> call = myApi.getTicketsByUser(user.getId());

        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.code() == 200) {
                    JsonElement loginResponse = response.body();

                    try {
                        JSONObject jsonObject = new JSONObject(loginResponse.toString());
                        JSONArray ticketsList = jsonObject.getJSONArray("tickets");


                        for (int i = 0; i < ticketsList.length(); i++) {
                            JSONObject tick = ticketsList.getJSONObject(i);

                            Ticket ticket = new Ticket(tick.getInt("id"),
                                    tick.getInt("idUser"),
                                    0,
                                    tick.getString("title"),
                                    tick.getString("subject"),
                                    tick.getString("status"),
                                    tick.getString("created_at"));

                            tickets.add(ticket);
                        }

                        recyclerView = view.findViewById(R.id.recylerview);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                        myTicketsAdapter = new MyTicketsAdapter(getActivity(), tickets);
                        recyclerView.setAdapter(myTicketsAdapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });

        return view;
    }
}
