package com.example.cowork_android.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cowork_android.Models.User;
import com.example.cowork_android.R;
import com.example.cowork_android.Retrofit.Api;
import com.example.cowork_android.Retrofit.RetrofitClient;
import com.example.cowork_android.Security.Md5;
import com.example.cowork_android.Storage.SharedPrefManager;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AccountFragment extends Fragment {

    Api myApi;
    EditText firstname, lastname, email, holdPassword, newPassword, reapeatNewPassword;
    Button  buttonChangeInformations, buttonChangePassword;
    Md5 md5 = new Md5();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account, container, false);

        //USER
        User user = SharedPrefManager.getInstance(getActivity()).getUser();

        // INIT API
        Retrofit retrofit = RetrofitClient.getInstance();
        myApi = retrofit.create(Api.class);

        //VIEW
        firstname = view.findViewById(R.id.firstname);
        lastname = view.findViewById(R.id.lastname);
        email = view.findViewById(R.id.email);
        holdPassword = view.findViewById(R.id.holdPassword);
        newPassword = view.findViewById(R.id.newPassword);
        reapeatNewPassword = view.findViewById(R.id.repeatNewPassword);
        buttonChangeInformations = view.findViewById(R.id.buttonChangeInformations);
        buttonChangePassword = view.findViewById(R.id.buttonChangePassword);

        firstname.setText(user.getFirstName());
        lastname.setText(user.getLastName());
        email.setText(user.getEmail());


        buttonChangeInformations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailPattern = "^[_A-Za-z0-9-]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})$";
                String fname = firstname.getText().toString().trim();
                String lname = lastname.getText().toString().trim();
                String mail = email.getText().toString().trim();

                if (fname.isEmpty()) {
                    firstname.setError("Saisir un prénom");
                    firstname.requestFocus();
                    return;
                }

                if (lname.isEmpty()) {
                    lastname.setError("Saisir un nom");
                    lastname.requestFocus();
                    return;
                }

                if (mail.isEmpty()) {
                    email.setError("Saisir une adresse email");
                    email.requestFocus();
                    return;
                }

                if (!mail.matches(emailPattern)) {
                    email.setError("L'adresse email n'est pas valide");
                    email.requestFocus();
                    return;
                }

                Call<JsonElement> call = myApi.updateUserInformations(user.getId(), fname, lname, mail);

                call.enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        if (response.code() == 200) {
                            JsonElement loginResponse = response.body();
                            try {
                                JSONObject jsonObject = new JSONObject(loginResponse.toString());
                                boolean success = jsonObject.getBoolean("success");

                                if (success){
                                    Toast.makeText(getActivity(), "Mise à jour effectuée", Toast.LENGTH_LONG).show();
                                    User u = new User(user.getId(),
                                            fname,
                                            lname,
                                            mail,
                                            user.getPassword(),
                                            user.getType());

                                    SharedPrefManager.getInstance(getActivity()).saveUser(u);
                                } else {
                                    Toast.makeText(getActivity(), "Impossible de mettre a jour", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        Toast.makeText(getActivity(), "404", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


        buttonChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hPassword = holdPassword.getText().toString().trim();
                String nPassword = newPassword.getText().toString().trim();
                String rnPassword = reapeatNewPassword.getText().toString().trim();

                if (hPassword.isEmpty()) {
                    holdPassword.setError("Saisir l'ancien mot de passe");
                    holdPassword.requestFocus();
                    return;
                }

                if (nPassword.isEmpty()) {
                    newPassword.setError("Saisir un nouveau mot de passe");
                    newPassword.requestFocus();
                    return;
                }

                if (rnPassword.isEmpty()) {
                    reapeatNewPassword.setError("Répéter le nouveau mot de passe");
                    reapeatNewPassword.requestFocus();
                    return;
                }

                hPassword = md5.md5(hPassword);

                if (hPassword.equals(user.getPassword())) {
                    if (nPassword.length() >= 8 && nPassword.length() <= 50) {
                        if (nPassword.equals(rnPassword)) {
                            String password = md5.md5(nPassword);
                            Call<JsonElement> call = myApi.updateUserPassword(user.getId(), password);

                            call.enqueue(new Callback<JsonElement>() {
                                @Override
                                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                                    if (response.code() == 200) {
                                        JsonElement loginResponse = response.body();
                                        try {
                                            JSONObject jsonObject = new JSONObject(loginResponse.toString());
                                            boolean success = jsonObject.getBoolean("success");

                                            if (success) {
                                                Toast.makeText(getActivity(), "Mise à jour effectuée", Toast.LENGTH_LONG).show();
                                                User u = new User(user.getId(),
                                                        user.getFirstName(),
                                                        user.getLastName(),
                                                        user.getEmail(),
                                                        password,
                                                        user.getType());

                                                SharedPrefManager.getInstance(getActivity()).saveUser(u);
                                            } else {
                                                Toast.makeText(getActivity(), "Impossible de mettre a jour", Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<JsonElement> call, Throwable t) {
                                    Toast.makeText(getActivity(), "404", Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            newPassword.setError("Les mots de passes ne coincident pas");
                            newPassword.requestFocus();
                        }
                    } else {
                        newPassword.setError("Le mot de passe ne respecte pas les règles de longueur");
                        newPassword.requestFocus();
                    }
                } else {
                    holdPassword.setError("Mot de passe incorrect");
                    holdPassword.requestFocus();
                }
            }
        });


        return view;
    }
}
