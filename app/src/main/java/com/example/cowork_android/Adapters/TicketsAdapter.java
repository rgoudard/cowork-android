package com.example.cowork_android.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cowork_android.Activities.TicketActivity;
import com.example.cowork_android.Models.Ticket;
import com.example.cowork_android.Models.User;
import com.example.cowork_android.R;
import com.example.cowork_android.Retrofit.Api;
import com.example.cowork_android.Retrofit.RetrofitClient;
import com.example.cowork_android.Storage.SharedPrefManager;
import com.google.gson.JsonElement;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TicketsAdapter extends RecyclerView.Adapter<TicketsAdapter.TicketsViewHolder> {

    Api myApi;
    private Context mCtx;
    private List<Ticket> tickets;

    public TicketsAdapter(Context mCtx, List<Ticket> tickets) {
        this.mCtx = mCtx;
        this.tickets = tickets;
    }

    @NonNull
    @Override
    public TicketsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mCtx);
        View view = layoutInflater.inflate(R.layout.tickets_card, null);
        return new TicketsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TicketsViewHolder ticketsViewHolder, int i) {
        Ticket ticket = tickets.get(i);

        ticketsViewHolder.title.setText(ticket.getTitle());
        ticketsViewHolder.subject.setText(ticket.getSubject());
        ticketsViewHolder.date.setText(ticket.getCreated_at());

        ticketsViewHolder.buttonAffect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // INIT API
                Retrofit retrofit = RetrofitClient.getInstance();
                myApi = retrofit.create(Api.class);

                User user = SharedPrefManager.getInstance(mCtx).getUser();

                Call<JsonElement> call = myApi.affectTicket(ticket.getId(), user.getId());

                call.enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        Toast.makeText(mCtx, "Ticket pris en charge", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(mCtx, TicketActivity.class);
                        intent.putExtra("ticketId", ticket.getId());
                        mCtx.startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        Toast.makeText(mCtx, "404", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

    }

    @Override
    public int getItemCount() {
        return tickets.size();
    }

    class TicketsViewHolder extends RecyclerView.ViewHolder{
        TextView title, subject, date;
        Button buttonAffect;


        TicketsViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            subject = itemView.findViewById(R.id.subject);
            date = itemView.findViewById(R.id.date);
            buttonAffect = itemView.findViewById(R.id.buttonAffect);

        }
    }
}
