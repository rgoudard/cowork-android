package com.example.cowork_android.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.cowork_android.Models.Message;
import com.example.cowork_android.Models.User;
import com.example.cowork_android.R;
import com.example.cowork_android.Retrofit.Api;
import com.example.cowork_android.Storage.SharedPrefManager;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {

    Api myApi;
    private Context mCtx;
    private List<Message> messages;


    public MessageAdapter(Context mCtx, List<Message> messages) {
        this.mCtx = mCtx;
        this.messages = messages;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mCtx);
        View view = layoutInflater.inflate(R.layout.message_card, null);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder messageViewHolder, int i) {

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) messageViewHolder.message.getLayoutParams();

        User user = SharedPrefManager.getInstance(mCtx).getUser();
        Message message = messages.get(i);

        if (message.getIdUser() == user.getId()){
            messageViewHolder.message.setBackgroundResource(R.drawable.background_users);
            messageViewHolder.message.setTextColor(Color.WHITE);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, -1);
            messageViewHolder.message.setLayoutParams(layoutParams);
            messageViewHolder.date.setGravity(Gravity.RIGHT);
        } else {
            messageViewHolder.message.setBackgroundResource(R.drawable.background_others);
            messageViewHolder.message.setTextColor(Color.BLACK);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            messageViewHolder.message.setLayoutParams(layoutParams);
            messageViewHolder.date.setGravity(Gravity.LEFT);

        }
        messageViewHolder.date.setText(message.getDate());
        messageViewHolder.message.setText(message.getMessage());
    }


    @Override
    public int getItemCount() {
        return messages.size();
    }

    class MessageViewHolder extends RecyclerView.ViewHolder{

        TextView date, message;

        MessageViewHolder(@NonNull View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.date);
            message = itemView.findViewById(R.id.message);
        }
    }
}
