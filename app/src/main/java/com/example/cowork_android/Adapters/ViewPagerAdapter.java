package com.example.cowork_android.Adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.cowork_android.Fragments.AccountFragment;
import com.example.cowork_android.Fragments.MyTicketsFragment;
import com.example.cowork_android.Fragments.TicketsFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new MyTicketsFragment();
            case 1:
                return new TicketsFragment();
            case 2:
                return new AccountFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "MES TICKETS";
            case 1:
                return "TICKETS EN ATTENTES";
            case 2:
                return "MON COMPTE";
        }
        return "Fragment " + position;
    }
}
