package com.example.cowork_android.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.cowork_android.Activities.TicketActivity;
import com.example.cowork_android.Models.Ticket;
import com.example.cowork_android.R;
import com.example.cowork_android.Retrofit.Api;

import java.util.List;

public class MyTicketsAdapter extends RecyclerView.Adapter<MyTicketsAdapter.MyTicketViewHolder> {

    Api myApi;
    private Context mCtx;
    private List<Ticket> tickets;

    public MyTicketsAdapter(Context mCtx, List<Ticket> tickets) {
        this.mCtx = mCtx;
        this.tickets = tickets;
    }

    @NonNull
    @Override
    public MyTicketViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mCtx);
        View view = layoutInflater.inflate(R.layout.my_tickets_card, null);
        return new MyTicketViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyTicketViewHolder myTicketViewHolder, int i) {
        Ticket ticket = tickets.get(i);

        myTicketViewHolder.title.setText(ticket.getTitle());
        myTicketViewHolder.subject.setText(ticket.getSubject());
        myTicketViewHolder.date.setText(ticket.getCreated_at());
        myTicketViewHolder.status.setText(ticket.getStatus());

        myTicketViewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mCtx, TicketActivity.class);
                intent.putExtra("ticketId", ticket.getId());
                mCtx.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return tickets.size();
    }

    class MyTicketViewHolder extends RecyclerView.ViewHolder{
        TextView title, subject, date, status;
        Button button;


        MyTicketViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            subject = itemView.findViewById(R.id.subject);
            date = itemView.findViewById(R.id.date);
            status = itemView.findViewById(R.id.status);

            button = itemView.findViewById(R.id.button);




        }
    }
}
