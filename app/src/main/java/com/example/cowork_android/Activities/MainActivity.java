package com.example.cowork_android.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.cowork_android.Adapters.ViewPagerAdapter;
import com.example.cowork_android.Models.User;
import com.example.cowork_android.R;
import com.example.cowork_android.Retrofit.Api;
import com.example.cowork_android.Retrofit.RetrofitClient;
import com.example.cowork_android.Storage.SharedPrefManager;

import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    Api myApi;
    Toolbar toolbar;
    ViewPager viewPager;
    ViewPagerAdapter adapter;
    TabLayout tabLayout;

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //USER
        User user = SharedPrefManager.getInstance(MainActivity.this).getUser();

        // INIT API
        Retrofit retrofit = RetrofitClient.getInstance();
        myApi = retrofit.create(Api.class);

        //VIEW
        toolbar = findViewById(R.id.toolBar);
        viewPager = findViewById(R.id.pager);
        tabLayout = findViewById(R.id.tabs);

        setSupportActionBar(toolbar);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

    }


    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() != 0){
            viewPager.setCurrentItem(0);
        } else {
            this.finish();
        }
    }

}
