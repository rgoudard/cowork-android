package com.example.cowork_android.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cowork_android.Models.User;
import com.example.cowork_android.R;
import com.example.cowork_android.Retrofit.Api;
import com.example.cowork_android.Retrofit.RetrofitClient;
import com.example.cowork_android.Security.Md5;
import com.example.cowork_android.Storage.SharedPrefManager;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    Api myApi;
    EditText emailText, passwordText;
    Button buttonSignin;
    Md5 md5 = new Md5();

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        // INIT API
        Retrofit retrofit = RetrofitClient.getInstance();
        myApi = retrofit.create(Api.class);


        //VIEW
        buttonSignin = findViewById(R.id.buttonSignin);
        emailText = findViewById(R.id.emailText);
        passwordText = findViewById(R.id.passwordText);


        buttonSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailText.getText().toString().trim();
                String password = passwordText.getText().toString().trim();

                if (email.isEmpty()) {
                    emailText.setError("Saisir un email");
                    emailText.requestFocus();
                    return;
                }

                if (password.isEmpty()) {
                    passwordText.setError("Saisir un mot de passe");
                    passwordText.requestFocus();
                    return;
                }

                Call<JsonElement> call = myApi.loginUser(email, md5.md5(password));

                call.enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        if (response.code() == 200) {
                            JsonElement loginResponse = response.body();
                            try {
                                JSONObject jsonObject = new JSONObject(loginResponse.toString());
                                boolean success = jsonObject.getBoolean("success");

                                if (success){
                                    Toast.makeText(LoginActivity.this, "Connexion", Toast.LENGTH_LONG).show();
                                    JSONArray users = jsonObject.getJSONArray("user");
                                    JSONObject user = users.getJSONObject(0);
                                    User u = new User(user.getInt("id"),
                                            user.getString("firstName"),
                                            user.getString("lastName"),
                                            user.getString("email"),
                                            user.getString("password"),
                                            user.getString("type"));

                                    SharedPrefManager.getInstance(LoginActivity.this).saveUser(u);

                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                } else {
                                    Toast.makeText(LoginActivity.this, "Identifiant ou mot de passe incorrect", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        Toast.makeText(LoginActivity.this, "404", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }



    @Override
    public void onBackPressed() {
        this.finish();
    }


}
