package com.example.cowork_android.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cowork_android.Adapters.MessageAdapter;
import com.example.cowork_android.Models.Message;
import com.example.cowork_android.Models.Ticket;
import com.example.cowork_android.Models.User;
import com.example.cowork_android.R;
import com.example.cowork_android.Retrofit.Api;
import com.example.cowork_android.Retrofit.RetrofitClient;
import com.example.cowork_android.Storage.SharedPrefManager;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TicketActivity extends AppCompatActivity {

    Api myApi;
    TextView name, subject, date, status;
    EditText newMessage;
    Button buttonSend;
    RecyclerView recyclerView;
    MessageAdapter messageAdapter;
    List<Message> messages = new ArrayList<>();

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ticket);
        Intent intent = this.getIntent();
        final int ticketId = intent.getIntExtra("ticketId", 1);

        //USER

        User user = SharedPrefManager.getInstance(TicketActivity.this).getUser();

        // INIT API
        Retrofit retrofit = RetrofitClient.getInstance();
        myApi = retrofit.create(Api.class);


        //VIEW
        name = findViewById(R.id.name);
        subject = findViewById(R.id.subject);
        date = findViewById(R.id.date);
        status = findViewById(R.id.status);
        newMessage = findViewById(R.id.newMessage);
        buttonSend = findViewById(R.id.buttonSend);

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = newMessage.getText().toString().trim();

                Call<JsonElement> call = myApi.createMessage(str, user.getId(), ticketId);

                call.enqueue(new Callback<JsonElement>() {

                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        finish();
                        startActivity(getIntent());
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        Toast.makeText(TicketActivity.this, "404", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


        Call<JsonElement> call = myApi.getTicketById(ticketId);

        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.code() == 200) {
                    JsonElement loginResponse = response.body();
                    try {
                        JSONObject jsonObject = new JSONObject(loginResponse.toString());
                        JSONArray tickets = jsonObject.getJSONArray("ticket");

                        JSONObject tick = tickets.getJSONObject(0);

                        Ticket ticket  = new Ticket( tick.getInt("id"),
                                tick.getInt("idUser"),
                                tick.getInt("idUserDev"),
                                tick.getString("title"),
                                tick.getString("subject"),
                                tick.getString("status"),
                                tick.getString("created_at"));

                        name.setText(ticket.getTitle());
                        subject.setText(ticket.getSubject());
                        date.setText(ticket.getCreated_at());
                        status.setText(ticket.getStatus());


                        Call<JsonElement> callMessages = myApi.getMessageByTicket(ticketId);

                        callMessages.enqueue(new Callback<JsonElement>() {

                            @Override
                            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                                if (response.code() == 200) {
                                    JsonElement loginResponse = response.body();

                                    try {
                                        JSONObject jsonObject = new JSONObject(loginResponse.toString());
                                        JSONArray messageList = jsonObject.getJSONArray("messages");

                                        for (int i = 0; i < messageList.length(); i++) {
                                            JSONObject mess = messageList.getJSONObject(i);

                                            Message message = new Message(mess.getInt("id"),
                                                    mess.getString("message"),
                                                    mess.getInt("idUser"),
                                                    mess.getInt("idTicket"),
                                                    mess.getString("date"));

                                            messages.add(message);

                                            recyclerView = findViewById(R.id.recylerview);
                                            recyclerView.setHasFixedSize(true);
                                            recyclerView.setLayoutManager(new LinearLayoutManager(TicketActivity.this));

                                            messageAdapter = new MessageAdapter(TicketActivity.this, messages);
                                            recyclerView.setAdapter(messageAdapter);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonElement> call, Throwable t) {

                            }
                        });



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Toast.makeText(TicketActivity.this, "404", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        this.finish();

    }
}
